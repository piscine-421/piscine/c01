/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/18 14:26:38 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <stdio.h>

void	ft_swap(int *a, int *b)
{
	int	swap[2];

	swap[0] = *a;
	swap[1] = *b;
	*a = swap[1];
	*b = swap[0];
}

/*int	main(void)
{
	int		nb1;
	int		nb2;

	nb1 = 3;
	nb2 = 7;
	ft_swap(&nb1, &nb2);
	printf("%d, %d\n", nb1, nb2);
}*/
