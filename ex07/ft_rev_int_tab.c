/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_int_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/18 12:19:24 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <stdio.h>

void	ft_rev_int_tab(int *tab, int size)
{
	int	index;
	int	howlong;
	int	startval;
	int	endval;

	index = 0;
	howlong = size / 2;
	while (howlong > 0)
	{
		size--;
		startval = tab[size];
		endval = tab[index];
		tab[size] = endval;
		tab[index] = startval;
		index++;
		howlong--;
	}
}

/*int	main(void)
{
	int	array[9] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
	int	i;
	int	size;

	i = 0;
	size = 9;
	ft_rev_int_tab(array, size);
	while (i != size)
	{
		printf("%d\n", array[i]);
		i++;
	}
}*/
